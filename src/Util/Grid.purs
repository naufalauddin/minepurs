module Util.Grid where

import Prelude

import Data.Array as A
import Data.FunctorWithIndex (class FunctorWithIndex, mapWithIndex)
import Data.Maybe (Maybe)
import Data.Newtype (class Newtype, unwrap, wrap)

newtype Grid a = Grid (Array (Array a))

type Coord = { row :: Int, col :: Int }

derive instance gridFunctor :: Functor Grid
derive instance gridNewtype :: Newtype (Grid a) _

instance gridIndexedFunctor :: FunctorWithIndex Coord Grid where
    mapWithIndex f = wrap <<< mapWithIndex (\row -> mapWithIndex \col -> f { row, col }) <<< unwrap

initialize :: forall a. Coord -> (Coord -> a) -> Grid a
initialize {row: rowRange, col: colRange} f = mapWithIndex (\coord _ -> f coord) $ emptyGrid {row: rowRange, col: colRange}
    where
        emptyGrid :: Coord -> Grid Unit
        emptyGrid {row, col} = wrap $ A.replicate row (A.replicate col unit)

index :: forall a. Grid a -> Coord -> Maybe a
index (Grid grid) {col, row} = grid A.!! row >>= (_ A.!! col)

infixl 8 index as !!

modifyAt :: forall a. Coord -> (a -> a) -> Grid a -> Grid a
modifyAt {row, col} f = wrap <<< A.modifyAtIndices [row] (A.modifyAtIndices [col] f) <<< unwrap

toGridOfIndex :: forall a. Grid a -> Grid Coord
toGridOfIndex = mapWithIndex const