module App.Board where

import Prelude

import Control.Monad.Gen.Trans (randomSample1, shuffle, suchThat)
import Control.Monad.Rec.Class as Rec
import Data.Array (concat, elem, foldr, intersect, length, notElem, take)
import Data.FunctorWithIndex (mapWithIndex)
import Data.List as L
import Data.Maybe (Maybe(..))
import Data.Newtype (unwrap)
import Data.Tuple.Nested ((/\))
import Effect.Aff.Class (class MonadAff)
import Halogen (ClassName(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Hooks as Hooks
import Util.Grid (Coord, Grid(..), modifyAt)
import Util.Grid as Grid
import Web.Event.Event as Event
import Web.UIEvent.MouseEvent as ME

boardSize ∷ { col ∷ Int , row ∷ Int }
boardSize = { row: 16, col: 30 }

data CellCondition
  = Closed
  | ClosedAndFlagged
  | Opened
data CellType
  = Mine
  | Safe Int
  | Unassigned
type Cell = { cellType :: CellType, condition :: CellCondition }

component
  :: forall q i o m
   . MonadAff m
  => H.Component q i o m
component = Hooks.component \_ _ -> Hooks.do
  board /\ boardId <- Hooks.useState (Grid.initialize boardSize $ const { cellType: Unassigned, condition: Closed})
  mines /\ minesId <- Hooks.useState []

  let
    regenMines clickCoord = do
      mines' <- H.liftEffect $ randomSample1 $ 
        (take 99 <$> shuffle (concat $ unwrap $ Grid.toGridOfIndex board))
          `suchThat` (notElem clickCoord) 
      let
        initiliazedBoard = Grid.initialize boardSize \coord ->
          let numberOfAdjacentMines = length $ adjacents coord `intersect` mines'
          in if coord `elem` mines'
            then { cellType: Mine, condition: Closed }
            else { cellType: Safe numberOfAdjacentMines, condition: Closed }

      Hooks.put minesId mines'
      Hooks.put boardId (openOnBoard [clickCoord] initiliazedBoard)

    restart = do
      Hooks.put boardId (Grid.initialize boardSize $ const { cellType: Unassigned, condition: Closed })

    open coord cell = do
      case cell of
        {cellType: Mine} -> Hooks.modify_ boardId $ openOnBoard mines
        {cellType: Unassigned} -> regenMines coord
        _ -> let allOpenedUpCoord = openUp board coord
             in Hooks.modify_ boardId $ openOnBoard allOpenedUpCoord

    flag coord = Hooks.modify_ boardId $ Grid.modifyAt coord (_ {condition = ClosedAndFlagged}) 

    onTileClicked coord cell event =
      case ME.button event of
        0 -> open coord cell
        2 -> do
          H.liftEffect $ Event.preventDefault (ME.toEvent event)
          flag coord
        _ -> pure unit

    tile coord cell = case cell of
      {cellType: Unassigned} -> HH.button [ HP.class_ (ClassName "tile"), HE.onClick (onTileClicked coord cell) ] [ HH.text "" ]
      {condition: Closed} -> HH.button [ HP.class_ (ClassName "tile"), HE.onClick (onTileClicked coord cell) ] [ HH.text "" ]
      {cellType: Mine} -> HH.button [ HP.class_ (ClassName "tile"), HP.disabled true, HE.onClick (onTileClicked coord cell) ] [ HH.text "*" ]
      {cellType: Safe x} -> HH.button [ HP.class_ (ClassName "tile"), HP.disabled true, HE.onClick (onTileClicked coord cell) ] [ HH.text $ if x == 0 then "" else show x ]

  Hooks.pure $
    HH.div_
      [ showGrid $ mapWithIndex tile board
      , HH.button [HE.onClick \_ -> restart ] [ HH.text "Restart Mines"] ]

adjacents :: Coord -> Array Coord
adjacents {row, col}
  = [ {row: row - 1, col: col - 1}
    , {row: row - 1, col}
    , {row: row - 1, col: col + 1}
    , {row: row, col: col - 1}
    , {row: row, col: col + 1}
    , {row: row + 1, col: col - 1}
    , {row: row + 1, col}
    , {row: row + 1, col: col + 1}
    ]

showGrid :: forall w i. Grid (HH.HTML w i) -> HH.HTML w i
showGrid (Grid grid) = HH.div_ $ map (HH.div [HP.class_ (ClassName "row")]) grid

openUp :: Grid Cell -> Coord -> Array Coord
openUp grid coord = Rec.tailRec go { queue: L.singleton coord , visited: L.Nil, result: L.Nil} 
  where
    go {queue: L.Nil, result} = Rec.Done (L.toUnfoldable result)
    go {queue: L.Cons c cs, visited, result} = if c `L.notElem` visited
      then case grid Grid.!! c of
        Just {cellType: Safe 0, condition: Closed} -> Rec.Loop {queue: cs <> (L.fromFoldable $ adjacents c), visited: L.Cons c visited, result: L.Cons c result}
        Just {cellType: Safe _, condition: Closed} -> Rec.Loop {queue: cs, visited: L.Cons c visited, result: L.Cons c result}
        _ -> Rec.Loop {queue: cs, visited, result}
      else Rec.Loop {queue: cs, visited, result}

openOnBoard :: Array Coord -> Grid Cell -> Grid Cell
openOnBoard coordToBeOpen board = foldr (\coord board' -> modifyAt coord (_ { condition = Opened}) board') board coordToBeOpen