{ name = "halogen-project"
, dependencies =
  [ "aff"
  , "arrays"
  , "console"
  , "effect"
  , "foldable-traversable"
  , "generate-values"
  , "halogen"
  , "halogen-hooks"
  , "lists"
  , "maybe"
  , "newtype"
  , "prelude"
  , "tailrec"
  , "tuples"
  , "web-events"
  , "web-uievents"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
